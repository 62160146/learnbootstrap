module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '/~cs62160146/learn_bootstrap/'
    : '/'
}
